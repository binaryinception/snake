module Snake {
    export class Game extends Phaser.Game {
        constructor() {
            super('100%', '100%', Phaser.AUTO, 'content', null);

            this.state.add('Boot', State.Boot, false);
            this.state.add('Level', State.Level, false);
            this.state.add('GameOver', State.GameOver, false);

            this.state.start('Boot');
        }
    }
}