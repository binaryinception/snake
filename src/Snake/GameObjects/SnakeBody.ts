module Snake.GameObjects {
    export class SnakeBody extends Phaser.Sprite {
        public map : Phaser.Tilemap;

        constructor(game : Phaser.Game, map: Phaser.Tilemap, x: number, y: number, imageKey : string = 'snake-body') {
            super(game,x, y, imageKey, 0);
            this.map = map;
            
            game.add.existing(this);

            game.physics.enable(this, Phaser.Physics.ARCADE);
        }

        getTilePosition():Phaser.Point {
            return new Phaser.Point(
                Math.floor(this.position.x / this.map.tileWidth),
                Math.floor(this.position.y / this.map.tileHeight)
            );
        }

        moveToTilePosition(point:Phaser.Point, tween:boolean = false):Phaser.Tween {
            return this.moveTo(
                point.x * this.map.tileWidth,
                point.y * this.map.tileHeight,
                tween
            );
        }

        moveTo(x : number, y : number, tween:boolean = false):Phaser.Tween {
            if (tween) {
                return this.game.add.tween(this).to({ x: x, y: y }, 250).start();
            }
            
            this.position.x = x;
            this.position.y = y;

            return null;
        }
    }
}