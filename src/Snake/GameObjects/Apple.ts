module Snake.GameObjects {
    export class Apple extends Phaser.Sprite {
        constructor(game:Phaser.Game, x: number, y: number, imageKey : string = 'apple') {
            super(game, x, y, imageKey);

            game.add.existing(this);

            game.physics.enable(this);

            this.events.onDestroy.add(() => {
                this.game.sound.play('apple-bite');
            });
        }
}
    }