module Snake.GameObjects {
    export class Map  {
        public tileMap   : Phaser.Tilemap
        public tileLayer : Phaser.TilemapLayer;
        public onCollide : Phaser.Signal;
        
        constructor(public game: Phaser.Game, public tileWidth: number, public tileHeight: number, public width: number, public height: number) {
            this.onCollide = new Phaser.Signal();

            // Some default settings
            this.tileMap = game.add.tilemap();
            this.tileMap.addTilesetImage('tileset', 'tileset', tileWidth, tileHeight, 0, 0, 0);
            
            let layer = this.tileLayer = this.tileMap.create('default', width, height, tileWidth, tileHeight);
            
            game.add.existing(layer);
            this.tileMap.currentLayer = 0;
            this.generateMap();
            
            this.tileMap.setCollision(2, true, this.tileLayer);
            
            layer.debug = true;
            // layer.resizeWorld();
            this.tileMap.setTileIndexCallback(2, () => {
                this.onCollide.dispatch(this);
            }, this)
            //this.game.physics.enable(this.tileLayer, Phaser.Physics.ARCADE);
        }

        generateMap():void {
            for (let x = 0; x < this.width; x++) {
                for (let y = 0; y < this.height; y++) {
                    if (x == 0 || x == this.width - 1 || y == 0 || y == this.height - 1) {
                        this.tileMap.putTile(2, x, y);
                    } else {
                        this.tileMap.putTile(3, x, y);
                    }
                }
            }
        }

        spawnApple():Apple {
            let x = Math.floor(Math.random() * this.width);
            let y = Math.floor(Math.random() * this.height);
            let t = this.tileMap.getTile(x, y);
            let i = 0;

            while (t.canCollide) {
                x = Math.floor(Math.random() * this.width);
                y = Math.floor(Math.random() * this.height);
                t = this.tileMap.getTile(x, y);

                i++;

                if (i > 30) {
                    console.log('too many retries, bugging out');
                    break;
                }
            }

            let apple = new Apple(this.game, x * this.tileWidth, y * this.tileHeight);

            return apple;
        }
    }
}