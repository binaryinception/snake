module Snake.GameObjects {
    export class SnakeHead extends SnakeBody {
        public currentDirection : Direction;
        public tail:SnakeBody[] = [];
        public onCollideWithSelf : Phaser.Signal;

        constructor(game : Phaser.Game, map: Phaser.Tilemap, x: number, y: number) {
            super(game, map, x, y, 'snake-head');
            this.currentDirection = Direction.Up;
            this.onCollideWithSelf = new Phaser.Signal();
        }

        nextTurn() {
            let pos = this.getTilePosition();
            let x = 0;
            let y = 0;

            switch (this.currentDirection) {
                case Direction.Up: 
                    y--;
                break;
                case Direction.Down:
                    y++;
                break;
                case Direction.Left:
                    x--;
                break;
                case Direction.Right:
                    x++;
                break;
            }

            for (let i = 0; i < this.tail.length; i++) {
                if (i == 0) {
                    this.tail[i].moveToTilePosition(pos, true);
                } else {
                    this.tail[i].moveToTilePosition(this.tail[i - 1].getTilePosition(), true);
                }
            }

            pos.x += x;
            pos.y += y;

            this.moveToTilePosition(pos, true);
        }

        lastBody():SnakeBody {
            if (this.tail.length == 0) {
                return this;
            }

            return this.tail[this.tail.length - 1];
        }

        addTail() {
            let body = new SnakeBody(this.game, this.map, this.lastBody().x, this.lastBody().y);
            
            this.tail.push(body);

            //this.addChild(body); // Then they are relative, not easy to handle.
        }

        update():void {
            if (this.tail.length <= 2) {
                // Dont check collisions before. Otherwise the head will always collide with first tail when they begin at the same place.
                return;
            }

            var collided = this.game.physics.arcade.collide(this, this.tail);
            
            this.game.debug.text(`Collision: Tail: ${collided}`, 0, 100, 'white');

            if (collided) {
                this.onCollideWithSelf.dispatch();
            }
        }
    }
}