module Snake.State {
    export class GameOver extends Phaser.State {

        create() {
            let text = this.game.add.text(this.game.world.centerX, this.game.world.centerY, "Game over!");

            text.addColor('#ffffff', 0);
            text.anchor.setTo(0.5, 0.5);
            text.scale.setTo(0.2, 0.2);

            this.game.sound.play('game-over');

            this.game.add.tween(text.scale)
                .to({ x: 8, y: 8 }, 10000, Phaser.Easing.Bounce.Out, true)
                .onComplete.add(() => {
                    this.game.state.start('Level');
                });
        }
    }
}