module Snake.State {
    export class Boot extends Phaser.State {
        preload() {
            console.log('preloading');

            // images
            this.game.load.image('logo', 'assets/images/apple.png');

            // sounds
            this.game.load.audio('apple-bite', 'assets/sounds/chomp.wav');
            this.game.load.audio('game-over', 'assets/sounds/crash.wav');
        }

        create() {
            console.log('create');
            var logo = this.game.add.sprite(this.game.world.centerX, this.game.world.centerY, 'logo');

            logo.anchor.setTo(0.5, 0.5);
            logo.scale.setTo(0.2, 0.2);

            this.game.add.tween(logo.scale)
                .to({ x: 10, y: 10 }, 2000, Phaser.Easing.Bounce.Out, true)
                .onComplete.add(() => {
                    this.game.state.start('Level');
                });
        }
    }
}