module Snake.State {
    export class Level extends Phaser.State {
        public worldMap : GameObjects.Map;
        public cursors : Phaser.CursorKeys;
        public sizeWidth : number = 10;
        public sizeHeight : number = 10;
        public tileWidth : number = 8;
        public tileHeight : number = 8;

        public player : GameObjects.SnakeHead;
        public apple  : GameObjects.Apple;
        public timer  : Phaser.Timer;

        preload() {
            this.game.load.image('tileset', 'assets/images/tileset.png');
            this.game.load.image('snake-head', 'assets/images/snake-body.png');
            this.game.load.image('snake-body', 'assets/images/snake-body.png');
            this.game.load.image('apple', 'assets/images/apple.png');
        }

        create() {
            this.game.world.scale.setTo(5, 5);

            // Physics
            this.game.physics.startSystem(Phaser.Physics.ARCADE);
            
            // Controls
            this.cursors = this.game.input.keyboard.createCursorKeys();

            // Level
            let map = this.worldMap = new GameObjects.Map(this.game, this.tileWidth, this.tileHeight, this.sizeWidth, this.sizeHeight);

            // Apple for the thirst
            let apple = this.apple = map.spawnApple();

            // Player
            this.player = new GameObjects.SnakeHead(this.game, map.tileMap, 0, 0);
            this.player.moveToTilePosition(new Phaser.Point(5,5));
            this.timer = this.game.time.create();

            let timerEvt = this.timer.loop(500, () => {
                this.player.nextTurn();
            }, this);

            this.player.onCollideWithSelf.add(() => this.gameOver());
            map.onCollide.add(() => {
                this.gameOver();
            });

            this.timer.start();
        }

        update() {
            if (!this.player || this.player.pendingDestroy) {
                return;
            }
            // let collidedWithWalls = this.game.physics.arcade.collide(this.player, this.worldMap.tileLayer, () => {
            //     console.log('Boink with wall')
            // });
            let collidedWithWalls = this.game.physics.arcade.collide(this.player, this.worldMap.tileLayer);
            
            if (collidedWithWalls) {
                this.player.destroy();
                return;
            }
            let collidedWithApple = this.game.physics.arcade.collide(this.player, this.apple);
            this.game.debug.text(`Collision: Walls: ${collidedWithWalls}  Apple: ${collidedWithApple}`, 0, 50, 'white');



            if (collidedWithApple) {
                this.apple.destroy();
                this.player.addTail();
                this.apple = this.worldMap.spawnApple();
            }

            switch (true) {
                case this.cursors.left.isDown: this.player.currentDirection = GameObjects.Direction.Left; break;
                case this.cursors.right.isDown: this.player.currentDirection = GameObjects.Direction.Right; break;
                case this.cursors.up.isDown: this.player.currentDirection = GameObjects.Direction.Up; break;
                case this.cursors.down.isDown: this.player.currentDirection = GameObjects.Direction.Down; break;
            }
        }

        gameOver() {
            console.info("Game Over!")
            this.game.state.start('GameOver')
        }
    }
}
